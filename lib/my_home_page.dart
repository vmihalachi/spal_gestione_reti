import 'package:flutter/material.dart';
import 'package:path/path.dart';
import 'package:spal_gestione_reti/consts.dart';
import 'package:spal_gestione_reti/pdf_mixin.dart';

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key}) : super(key: key);

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> with PDFMixin {
  int bottomBarIndex = 0;

  Widget buildHeader() => Column(
        children: [
          Container(
              padding: EdgeInsets.only(bottom: 12.0),
              color: kPrimaryColor,
              width: double.infinity,
              child: Text(
                'Powered by Tecnograph',
                textAlign: TextAlign.center,
                style: TextStyle(fontWeight: FontWeight.bold, color: Colors.white),
              )),
          SizedBox(height: 20.0),
        ],
      );

  Widget buildBottomSpace() => SizedBox(height: 100.0);

  static final kIconPdf = Icon(Icons.insert_drive_file, color: Colors.redAccent);
  static final kIconFolder = Icon(Icons.folder);

  Widget buildFileListTile(String name) =>
      ListTile(title: Text(basename(name).replaceAll(RegExp(r'(_|-|\.pdf)'), ' ')), onTap: () => apriPdf(name));

  List<String> createArrayWithCommonStart(String commonStart, List<String> array) =>
      array.map((it) => '$commonStart/$it').toList();

  ListView buildFilesList() => ListView(
        physics: ClampingScrollPhysics(),
        children: [
          buildHeader(),
          // liste espandibile per i documenti
          ExpansionTile(title: Text('Rete dati, audio, video e videosorveglianza bar'), leading: kIconPdf, children: [
            // rd00
            ExpansionTile(
                title: Text('RD00 - Locale caldaia'),
                leading: kIconFolder,
                children: createArrayWithCommonStart('rete_dati/RD00', [
                  'RD00-SCHEMA_LOGICO',
                  'RD00_FRONTALE_RACK',
                  'RD00a_CHASSIS',
                  'RD00b_CHASSIS'
                ]).map(buildFileListTile).toList()),
            // rd01
            ExpansionTile(
                title: Text('RD01 - Area tecnica'),
                leading: kIconFolder,
                children: createArrayWithCommonStart('rete_dati/RD01', ['RD01-SCHEMA_LOGICO', 'RD01_FRONTALE_RACK'])
                    .map(buildFileListTile)
                    .toList()),
            // rd02
            ExpansionTile(
                title: Text('RD02 - Posti polizia'),
                leading: kIconFolder,
                children: createArrayWithCommonStart('rete_dati/RD02', ['RD02-SCHEMA_LOGICO', 'RD02_FRONTALE_RACK'])
                    .map(buildFileListTile)
                    .toList()),
            // rd03
            ExpansionTile(
                title: Text('RD03 - Curva ovest'),
                leading: kIconFolder,
                children: createArrayWithCommonStart(
                        'rete_dati/RD03', ['RD03-SCHEMA_LOGICO', 'RD03_CHASSIS', 'RD03_FRONTALE_RACK'])
                    .map(buildFileListTile)
                    .toList()),
            // rd04
            ExpansionTile(
                title: Text('RD04 - Locale 118'),
                leading: kIconFolder,
                children: createArrayWithCommonStart('rete_dati/RD04', ['RD04-SCHEMA_LOGICO', 'RD04_FRONTALE_RACK'])
                    .map(buildFileListTile)
                    .toList()),
            // rd05
            ExpansionTile(
                title: Text('RD05 - Cabina regia'),
                leading: kIconFolder,
                children: createArrayWithCommonStart(
                        'rete_dati/RD05', ['RD05-SCHEMA_LOGICO', 'RD05_CHASSIS', 'RD05_FRONTALE_RACK'])
                    .map(buildFileListTile)
                    .toList()),
            // rd06
            ExpansionTile(
                title: Text('RD06 - Biglietteria via Fortezza'),
                leading: kIconFolder,
                children: createArrayWithCommonStart('rete_dati/RD06', ['RD06-SCHEMA_LOGICO', 'RD06_FRONTALE_RACK'])
                    .map(buildFileListTile)
                    .toList()),
            // rd07
            ExpansionTile(
                title: Text('RD07 - Tribuna nord'),
                leading: kIconFolder,
                children: createArrayWithCommonStart('rete_dati/RD07', ['RD07-SCHEMA_LOGICO', 'RD07_FRONTALE_RACK'])
                    .map(buildFileListTile)
                    .toList()),
            // rd08
            ExpansionTile(
                title: Text('RD08 - Corridoio tribuna sud'),
                leading: kIconFolder,
                children: createArrayWithCommonStart('rete_dati/RD08', ['RD08-SCHEMA_LOGICO'])
                    .map(buildFileListTile)
                    .toList()),
            // rd09
            ExpansionTile(
                title: Text('RD09 - Bordo campo'),
                leading: kIconFolder,
                children: createArrayWithCommonStart('rete_dati/RD09', ['RD09-SCHEMA_LOGICO', 'RD09_FRONTALE_RACK'])
                    .map(buildFileListTile)
                    .toList()),
            // rd10
            ExpansionTile(
                title: Text('RD10 - Ufficio videosorveglianza bar'),
                leading: kIconFolder,
                children: createArrayWithCommonStart('rete_dati/RD10', ['RD10-SCHEMA_LOGICO', 'RD10_FRONTALE_RACK'])
                    .map(buildFileListTile)
                    .toList()),
            // altri file
            buildFileListTile('rete_dati/TAV22_PLANIMETRIA'),
            buildFileListTile('rete_dati/TAV23_PLANIMETRIA_BIGLIETTERIE'),
            buildFileListTile('rete_dati/TAV27_SCHEMI_OTTICI'),
            buildFileListTile('rete_dati/TAV30_PIANO_POSA_CAVI'),
            buildFileListTile('rete_dati/TAV31_DISTR_SECONDARIA_LAN_VIDEOSORV_BAR'),
            buildFileListTile('rete_dati/TAV34_SCHEMA_RACK'),
          ]),
          // rete glt var
          ExpansionTile(title: Text('Rete GLT-VAR'), leading: kIconPdf, children: [
            // cabinet 1
            ExpansionTile(
                title: Text('Cabinet 1'),
                leading: kIconFolder,
                children: createArrayWithCommonStart('rete_glt_var/cabinet_1', ['CABINET_1_FRONTALE_RACK'])
                    .map(buildFileListTile)
                    .toList()),
            // cabinet 2
            ExpansionTile(
                title: Text('Cabinet 2'),
                leading: kIconFolder,
                children: createArrayWithCommonStart('rete_glt_var/cabinet_2', ['CABINET_2_FRONTALE_RACK'])
                    .map(buildFileListTile)
                    .toList()),
            // ob cabinet
            ExpansionTile(
                title: Text('OB Cabinet'),
                leading: kIconFolder,
                children: createArrayWithCommonStart('rete_glt_var/ob_cabinet', ['OB_CABINET_FRONTALE_RACK'])
                    .map(buildFileListTile)
                    .toList()),
            // rra
            ExpansionTile(
                title: Text('RRA'),
                leading: kIconFolder,
                children: createArrayWithCommonStart('rete_glt_var/rra', ['RRA_FRONTALE_RACK'])
                    .map(buildFileListTile)
                    .toList()),
            // vor
            ExpansionTile(
                title: Text('VOR'),
                leading: kIconFolder,
                children: createArrayWithCommonStart('rete_glt_var/vor', ['VOR_FRONTALE_RACK'])
                    .map(buildFileListTile)
                    .toList()),
            // altri file
            buildFileListTile('rete_glt_var/TAV26_PLANIMETRIA'),
            buildFileListTile('rete_glt_var/TAV29_SCHEMI_OTTICI'),
            buildFileListTile('rete_glt_var/TAV33_PIANO_POSA_CAVI'),
            buildFileListTile('rete_glt_var/TAV36_SCHEMA_RACK'),
          ]),
          // rete led
          ExpansionTile(title: Text('Rete LED'), leading: kIconPdf, children: [
            // rl00
            ExpansionTile(
                title: Text('RL00'),
                leading: kIconFolder,
                children: createArrayWithCommonStart('rete_led/RL00', ['RL00_FRONTALE_RACK'])
                    .map(buildFileListTile)
                    .toList()),
            // rl01
            ExpansionTile(
                title: Text('RL01'),
                leading: kIconFolder,
                children: createArrayWithCommonStart('rete_led/RL01', ['RL01_FRONTALE_RACK'])
                    .map(buildFileListTile)
                    .toList()),
            // rl02
            ExpansionTile(
                title: Text('RL02'),
                leading: kIconFolder,
                children: createArrayWithCommonStart('rete_led/RL02', ['RL02_FRONTALE_RACK'])
                    .map(buildFileListTile)
                    .toList()),
            // rl03
            ExpansionTile(
                title: Text('RL03'),
                leading: kIconFolder,
                children: createArrayWithCommonStart('rete_led/RL03', ['RL03_FRONTALE_RACK'])
                    .map(buildFileListTile)
                    .toList()),
            // altri file
            buildFileListTile('rete_led/TAV24_PLANIMETRIA_1'),
            buildFileListTile('rete_led/TAV25_PLANIMETRIA_2'),
            buildFileListTile('rete_led/TAV28_SCHEMI_OTTICI'),
            buildFileListTile('rete_led/TAV32_PIANO_POSA_CAVI'),
            buildFileListTile('rete_led/TAV35_SCHEMA_RACK'),
          ]),
          // altri file
          buildFileListTile('TAV21_ELENCO_ELABORATI'),
          buildBottomSpace()
        ],
      );

  ListTile buildDevicesListTile(String name) {
    String fileName = '$name-SCHEMA_LOGICO';
    return ListTile(
        title: Text(name),
        trailing: Row(
            mainAxisSize: MainAxisSize.min,
            children: [FlatButton(child: Text('Apri PDF'.toUpperCase()), onPressed: () => apriPdf(fileName))]));
  }

  /*Widget buildDevicesPage() => ListView(children: [
        buildHeader(),
        buildDevicesListTile('RD01'),
        Divider(),
        buildDevicesListTile('RD02'),
        Divider(),
        buildDevicesListTile('RD03'),
        Divider(),
        buildDevicesListTile('RD04'),
        Divider(),
        buildDevicesListTile('RD05'),
        Divider(),
        buildDevicesListTile('RD06'),
        Divider(),
        buildDevicesListTile('RD07'),
        Divider(),
        buildDevicesListTile('RD08'),
        Divider(),
        buildDevicesListTile('RD09'),
        Divider(),
        buildDevicesListTile('RD10'),
        Divider(),
        buildBottomSpace()
      ]);*/

  Widget buildInfoText() => Column(
        children: [
          buildHeader(),
          Padding(
            padding: EdgeInsets.all(8.0),
            child: Text(
              'Questa applicazione serve per facilitare la gestione delle reti dello stadio Paolo Mazza.',
              style: TextStyle(fontSize: 16.0),
            ),
          ),
        ],
      );

  BottomNavigationBar buildBottomNavigationBar() => BottomNavigationBar(
          onTap: (index) {
            setState(() {
              bottomBarIndex = index;
            });
          },
          currentIndex: bottomBarIndex,
          items: [
            BottomNavigationBarItem(title: Text('File'), icon: Icon(Icons.insert_drive_file)),
            // BottomNavigationBarItem(title: Text('Dispositivi'), icon: Icon(Icons.signal_wifi_4_bar)),
            BottomNavigationBarItem(title: Text('Info'), icon: Icon(Icons.info_outline))
          ]);

  /// ritorna il contenuto del body a un certo index
  Widget getPageAtIndex(int index) {
    switch (index) {
      case 0:
        return buildFilesList();
      // case 1:
      //  return buildDevicesPage();
      // case 2:
      default:
        return buildInfoText();
    }
  }

  @override
  Widget build(BuildContext context) => Scaffold(
      appBar: AppBar(centerTitle: true, elevation: 0.0, title: Text(kAppTitle)),
      bottomNavigationBar: buildBottomNavigationBar(),
      // pulsante per scanerizzare qr
      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.camera_alt),
        tooltip: 'Scanerizza codice QR',
        onPressed: () => scan(context),
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,
      body: getPageAtIndex(bottomBarIndex));
}
