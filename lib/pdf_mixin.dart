import 'dart:async';

import 'package:barcode_scan/barcode_scan.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_pdf_viewer/flutter_pdf_viewer.dart';
import 'package:spal_gestione_reti/consts.dart';
import 'package:spal_gestione_reti/modal_messaggio.dart';

class PDFMixin {
  Future<void> apriPdf(String fileName) {
    // se il nome non finisce con l estensione pdf la aggiungiamo
    if (!fileName.endsWith('.pdf')) fileName = '$fileName.pdf';
    return FlutterPdfViewer.loadAsset('assets/pdf/$fileName');
  }

  Future scan(BuildContext context) async {
    // messaggio all utente
    String msgModalBarcode;
    try {
      String barcode = await BarcodeScanner.scan();
      String pathCompleto = pdfs.firstWhere((it) => it.contains(barcode));
      apriPdf(pathCompleto);
    } on PlatformException catch (e) {
      if (e.code == BarcodeScanner.CameraAccessDenied) {
        msgModalBarcode = "Non hai dato il permesso di usare la fotocamera";
      } else {
        msgModalBarcode = 'Errore sconosciuto: $e';
      }
    } on FormatException {
      // msgModalBarcode = "L'utente è tornato indietro";
    } catch (e) {
      msgModalBarcode = 'Errore sconosciuto: $e';
    }
    // mostriamo il modal se abbiamo qualcosa da dire
    if (msgModalBarcode != null) showMessaggioModal(context, msgModalBarcode);
  }
}
