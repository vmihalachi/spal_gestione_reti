import 'package:flutter/material.dart';

import 'package:spal_gestione_reti/consts.dart';
import 'package:spal_gestione_reti/login_page.dart';
import 'package:spal_gestione_reti/my_home_page.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: kAppTitle,
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
          primaryColor: kPrimaryColor,
          accentColor: kAccentColor,
          brightness: Brightness.dark,
          inputDecorationTheme: InputDecorationTheme(
              contentPadding: EdgeInsets.all(14.0),
              border: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(20.0))),
          buttonTheme: ButtonThemeData(
            padding: EdgeInsets.all(14.0),
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(20.0)),
          )),
      home: LoginPage(),
    );
  }
}
