import 'dart:async';

import 'package:flutter/material.dart';

Future<Null> showMessaggioModal(BuildContext context, String messaggio) async {
  await showDialog<Null>(
      context: context,
      builder: (BuildContext context) => SimpleDialog(
            contentPadding: EdgeInsets.all(24.0),
            title: Text('Messaggio'),
            children: [
              Text(messaggio),
              SizedBox(height: 24.0),
              OutlineButton(
                  child: Text('Tutto chiaro!'),
                  onPressed: () => Navigator.pop(context)),
            ],
          ));
}
