import 'package:flutter/material.dart';
import 'package:spal_gestione_reti/consts.dart';
import 'package:spal_gestione_reti/my_home_page.dart';

class LoginPage extends StatefulWidget {
  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  final _formKey = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(centerTitle: true, elevation: 0.0, title: Text(kAppTitle)),
      body: Container(
        decoration: BoxDecoration(
            image: DecorationImage(
          colorFilter: ColorFilter.mode(
              Color.fromRGBO(0, 0, 0, 0.85), BlendMode.multiply),
          image: AssetImage("assets/img/stadium-image.jpg"),
          fit: BoxFit.cover,
        )),
        padding: EdgeInsets.all(24.0),
        child: Form(
            key: _formKey,
            child: ListView(children: [
              Padding(
                padding: EdgeInsets.symmetric(horizontal: 24.0, vertical: 50.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Image.asset(
                      'assets/img/logo_spal.png',
                      width: 80.0,
                      fit: BoxFit.cover,
                    ),
                    Image.asset(
                      'assets/img/tenograph_logo.png',
                      width: 120.0,
                      fit: BoxFit.cover,
                    ),
                  ],
                ),
              ),
              // titolo login
              Text(
                'Login',
                textAlign: TextAlign.center,
                style: TextStyle(fontSize: 36.0, fontWeight: FontWeight.bold),
              ),
              SizedBox(height: 30.0),
              // input per il codice di accesso
              TextFormField(
                // autofocus: true,
                decoration: InputDecoration(hintText: "Codice d'accesso"),
                validator: (value) {
                  /*if (value.isEmpty) {
                    return 'Non deve essere vuoto';
                  }*/
                },
              ),
              SizedBox(height: 10.0),
              // pulsante per fare il submit
              SizedBox(
                width: double.infinity,
                child: RaisedButton(
                  child: Text('Entra'),
                  color: kPrimaryColor,
                  onPressed: () {
                    if (_formKey.currentState.validate()) {
                      Navigator.pushReplacement(context,
                          MaterialPageRoute(builder: (_) => MyHomePage()));
                    }
                  },
                ),
              )
            ])),
      ),
    );
  }
}
